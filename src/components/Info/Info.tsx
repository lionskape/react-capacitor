import React, { useCallback } from "react";
import { Plugins, Capacitor } from "@capacitor/core";
import { ShareIcon } from "./share";

const { Share } = Plugins;

interface InfoProps {
  weight: string;
  height: string;
  id: string;
  date: string;
  bmi: string;
  deleteCard: (id: string) => void;
}

// @ts-ignore
const shareAvailable = navigator.share || Capacitor.isNative;

const Info: React.FC<InfoProps> = ({
  weight,
  height,
  id,
  date,
  bmi,
  deleteCard
}) => {
  const handleDelete = useCallback(() => {
    deleteCard(id);
  }, [deleteCard, id]);

  const handleShare = useCallback(async () => {
    return await Share.share({
      title: `My BMI is ${bmi}`,
      text: "Let's check your BMI?",
      url: "https://vladimir-ulyanov.ru",
      dialogTitle: "Share your BMI"
    });
  }, [bmi]);

  return (
    <div className="col m6 s12">
      <div className="card">
        <div className="card-content">
          <span className="card-title" data-test="bmi">
            BMI: {bmi}
          </span>
          <div className="card-data">
            <span data-test="weight">Weight: {weight} kg</span>
            <span data-test="height">Height: {height} cm</span>
            <span data-test="date">Date: {date}</span>
          </div>
          <div className="right">
            {shareAvailable && (
              <button className="btn-floating grey" onClick={handleShare}>
                <ShareIcon size={15} />
              </button>
            )}
            <button className="btn-floating red" onClick={handleDelete}>
              X
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Info;

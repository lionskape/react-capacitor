import React, { useState, useEffect } from "react";
import BmiForm from "../BmiForm/BmiForm";
import Info from "../Info/Info";
import Bar from "../Bar/Bar";
import "./App.css";
import "materialize-css/dist/css/materialize.min.css";

import { BmiData } from "../bmiData";

import nanoId from "nanoid";

const localStorage = window.localStorage;

const initialState = (): Array<BmiData> => {
  const data = localStorage.getItem("data");
  return data ? JSON.parse(data) || [] : [];
};

interface DataState {
  date: Array<string>;
  bmi: Array<string>;
}

const App = () => {
  const [state, setState] = useState(initialState);
  const [data, setData] = useState<DataState>({
    date: [],
    bmi: []
  });

  const handleChange = (val: {
    weight: number;
    height: number;
    date: string;
  }) => {
    let heightInM = val.height / 100;

    let newVal = [
      ...state,
      {
        ...val,
        id: nanoId(),
        bmi: (val.weight / (heightInM * heightInM)).toFixed(2)
      }
    ];

    let len = newVal.length;
    if (len > 7) newVal = newVal.slice(1, len);
    setState(newVal);
  };

  const handleDelete = (id: string) => {
    localStorage.setItem("lastState", JSON.stringify(state));
    setState(
      state.filter(bmiData => {
        return bmiData.id !== id;
      })
    );
  };
  const handleUndo = () => {
    setState(JSON.parse(localStorage.getItem("lastState")!));
  };
  useEffect(() => {
    localStorage.setItem("data", JSON.stringify(state));

    const date = state.map(obj => obj.date);
    const bmi = state.map(obj => obj.bmi);

    setData({ date, bmi });
  }, [state]);

  return (
    <div className="container">
      <div className="row center">
        <h1 className="white-text"> BMI Tracker </h1>
      </div>
      <div className="row">
        <div className="col m12 s12">
          <BmiForm change={handleChange} />
          <Bar labelData={data.date} bmiData={data.bmi} />
          <div>
            <div className="row center">
              <h4 className="white-text">7 Day Data</h4>
            </div>
            <div className="data-container row">
              {state.length > 0 ? (
                <>
                  {state.map(info => (
                    <Info
                      key={info.id}
                      id={info.id}
                      weight={info.weight.toString()}
                      height={info.height.toString()}
                      date={info.date}
                      bmi={info.bmi}
                      deleteCard={handleDelete}
                    />
                  ))}
                </>
              ) : (
                <div className="center white-text">No log found</div>
              )}
            </div>
          </div>
          {localStorage.getItem("lastState") !== null ? (
            <div className="center">
              <button className="calculate-btn" onClick={handleUndo}>
                Undo
              </button>
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    </div>
  );
};

export default App;

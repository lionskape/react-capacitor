import React, { useState } from "react";
import "../App/App.css";

interface BmiFormProps {
  change: (state: State) => void;
}

interface State {
  weight: number;
  height: number;
  date: string;
}

const BmiForm: React.FC<BmiFormProps> = ({ change }) => {
  const [state, setState] = useState({
    weight: "",
    height: "",
    date: ""
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = Number(e.target.value);
    if (value > 999) {
      value = 999;
    }

    setState({
      ...state,
      [e.target.name]: value,
      date: new Date().toLocaleString().split(",")[0]
    });
  };

  const handleSubmit = () => {
    change({
      weight: Number(state.weight),
      height: Number(state.height),
      date: state.date
    });
    setState({
      weight: "",
      height: "",
      date: ""
    });
  };

  return (
    <>
      <div className="row">
        <div className="col m6 s12">
          <label htmlFor="weight">Weight (in kg)</label>
          <input
            id="weight"
            name="weight"
            type="number"
            min="1"
            max="999"
            placeholder="50"
            value={state.weight}
            onChange={handleChange}
          />
        </div>

        <div className="col m6 s12">
          <label htmlFor="height">Height (in cm)</label>
          <input
            id="height"
            name="height"
            type="number"
            min="1"
            max="999"
            placeholder="176"
            value={state.height}
            onChange={handleChange}
          />
        </div>
      </div>
      <div className="center">
        <button
          id="bmi-btn"
          className="calculate-btn"
          type="button"
          disabled={state.weight === "" || state.height === ""}
          onClick={handleSubmit}
        >
          Calculate BMI
        </button>
      </div>
    </>
  );
};

export default BmiForm;

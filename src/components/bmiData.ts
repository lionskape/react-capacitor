export interface BmiData {
  date: string;
  weight: number;
  height: number;
  bmi: string;
  id: string;
}
